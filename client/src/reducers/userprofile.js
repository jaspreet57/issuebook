import Immutable from 'immutable';

const initialState = Immutable.Map();

export default (state = initialState, action) => {
	switch (action.type) {
      
		case 'GET_USER_PROFILE_SUCCESS': {
			return state.merge({user: action.user});
		}
      
		case 'GET_USER_PROFILE_FAILURE': {
			return state.clear();
		}
      
      
   	case 'DELETE_ASSIGNMENT_SUCCESS':
		case 'GET_ASSIGNMENTS_SUCCESS': {
			return state.merge({assignments: action.assignments});
		}

		case 'DELETE_ASSIGNMENT_FAILURE':
		case 'GET_ASSIGNMENT_FAILURE': {
			return state.clear();
		}
    
		default:
			return state;
	}
}