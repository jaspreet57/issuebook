import Immutable from 'immutable';

const initialState = Immutable.Map();

export default (state = initialState, action) => {
	switch (action.type) {
		case 'DELETE_BOOK_SUCCESS':
		case 'GET_BOOKS_SUCCESS': {
			return state.merge({list: action.books});
		}

		case 'DELETE_BOOK_FAILURE':
		case 'GET_BOOKS_FAILURE': {
			return state.clear();
		}

		case 'SET_BOOK_SEARCH_BAR': {
			return state.merge({searchBar: action.keyword});
		}

		default:
			return state;
	}
}