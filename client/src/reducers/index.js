import { combineReducers } from 'redux-immutable';

import {reducer as form} from 'redux-form/immutable';
import books from './books';
import users from './users';
import userprofile from './userprofile';

export default combineReducers({
	books,
	users,
	userprofile,
	form
});