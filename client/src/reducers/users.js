import Immutable from 'immutable';

const initialState = Immutable.Map();

export default (state = initialState, action) => {
	switch (action.type) {
		case 'DELETE_USER_SUCCESS':
		case 'GET_USERS_SUCCESS': {
			return state.merge({list: action.users});
		}

		case 'DELETE_USER_FAILURE':
		case 'GET_USERS_FAILURE': {
			return state.clear();
		}

		case 'SET_USER_SEARCH_BAR': {
			return state.merge({searchBar: action.keyword});
		}

		default:
			return state;
	}
}