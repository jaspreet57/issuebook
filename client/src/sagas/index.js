import {
	watchGetBooks,
	watchDeleteBook,
	watchPostBook,
	watchPostAssign
} from './books';

import {
	watchGetUsers,
	watchDeleteUser,
	watchPostUser
} from './users';


import {
	watchGetUserProfile,
  watchGetAssignments,
  watchDeleteAssignment
} from './userprofile';


export default function* rootSaga(){
	yield [
		watchGetBooks(),
		watchDeleteBook(),
		watchPostBook(),
		watchPostAssign(),
		watchGetUsers(),
		watchDeleteUser(),
		watchPostUser(),
		watchGetUserProfile(),
  	watchGetAssignments(),
  	watchDeleteAssignment()
	];
}