import {takeLatest} from 'redux-saga';

// below are some saga effects

import {put, select, call} from 'redux-saga/effects';

import {getUserProfileSuccess, getUserProfileFailure, getAssignmentsSuccess, getAssignmentsFailure, deleteAssignmentSuccess, deleteAssignmentFailure} from '../actions/userprofile';


const fetchUser = (id) => {
	  return fetch(`http://localhost:8080/users/${id}`, {
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		})
		.then(response => response.json());
};

const fetchAssignments = (id) => {
	  return fetch(`http://localhost:8080/assign/user/${id}`, {
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		})
		.then(response => response.json());
};


const selectedAssignments = (state) => {
	return state.getIn(['userprofile', 'assignments']).toJS();
};


const deleteServerAssignment = (id) => {
	return fetch(`http://localhost:8080/assign/${id}`, {
			headers: new Headers({
				'Content-Type': 'application/json'
			}),
			method: 'DELETE',
		})
		.then(response => response.json());
};




function* getUser(action){
	try {
		const user = yield call(fetchUser, action.id);
		yield put(getUserProfileSuccess(user));
	}catch (err){
		yield put(getUserProfileFailure());
	}
}


function* getAssignments(action){
  try{
    const assignments = yield call(fetchAssignments, action.id);
    yield put(getAssignmentsSuccess(assignments));
  }catch(err){
    yield put(getAssignmentsFailure);
  }
}


function* deleteAssignment (action){
	const {id} = action;
	const assignments = yield select(selectedAssignments);
	try {
		yield call(deleteServerAssignment, id);

		yield put(deleteAssignmentSuccess(assignments.filter(assignment => assignment._id !== id)));
	} catch (e) {
		yield put(deleteAssignmentFailure());
	}
}



function* watchGetUserProfile(){
	yield takeLatest('GET_USER_PROFILE', getUser);
}

function* watchGetAssignments(){
	yield takeLatest('GET_ASSIGNMENTS', getAssignments);
}


function* watchDeleteAssignment() {
	yield takeLatest('DELETE_ASSIGNMENT', deleteAssignment);
}

export {
	watchGetUserProfile,
  watchGetAssignments,
  watchDeleteAssignment
};