import {takeLatest} from 'redux-saga';

// below are some saga effects

import {put, select, call} from 'redux-saga/effects';

import {getBooks as getBooksAction, getBooksSuccess, getBooksFailure, deleteBookSuccess, deleteBookFailure, postBookSuccess, postBookFailure} from '../actions/books';
import {postAssignFailure} from '../actions/assign';

const fetchBooks = () => {
	return fetch('http://localhost:8080/books', {
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		})
		.then(response => response.json())
};


const selectedBooks = (state) => {
	return state.getIn(['books', 'list']).toJS();
};


const deleteServerBook = (id) => {
	return fetch(`http://localhost:8080/books/${id}`, {
			headers: new Headers({
				'Content-Type': 'application/json'
			}),
			method: 'DELETE',
		})
		.then(response => response.json());
};

const postServerBook = (book) => {
	return  fetch('http://localhost:8080/books', {
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				method: 'POST',
				body: JSON.stringify(book)
			})
			.then(response => response.json());
};

const postServerAssign = (assignment)=>{
	return fetch('http://localhost:8080/assign', {
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
      method: 'POST',
      body: JSON.stringify(assignment)
    })
    .then(response => response.json());
};


const getBookForm = (state) => {
	return state.getIn(['form', 'book']).toJS();
}


function* getBooks(){
	try {
		const books = yield call(fetchBooks);
		yield put(getBooksSuccess(books));
	}catch (err){
		yield put(getBooksFailure());
	}
}


function* deleteBook (action){
	const {id} = action;
	const books = yield select(selectedBooks);
	try {
		yield call(deleteServerBook, id);

		yield put(deleteBookSuccess(books.filter(book => book._id !== id)));
	} catch (e) {
		yield put(deleteBookFailure());
	}
}


function* postBook(){
	const book = yield select(getBookForm);

	const newBook = Object.assign({}, book.values);

	try {
		yield call(postServerBook, newBook);
		yield put(getBooksAction());
	}catch (e){
		yield put(postBookFailure());
	}
}


function* postAssign(action){
	const newAssignment = {userid: action.userid, bookid: action.bookid};
	try{
		const data = yield call(postServerAssign, newAssignment);
		alert(data.message);
		yield put(getBooksAction());
	}catch(e){
		yield put(postAssignFailure());
	}
}



function* watchGetBooks(){
	yield takeLatest('GET_BOOKS', getBooks);
}


function* watchDeleteBook() {
	yield takeLatest('DELETE_BOOK', deleteBook);
}


function* watchPostBook(){
	yield takeLatest('POST_BOOK', postBook);
}

function* watchPostAssign(){
	yield takeLatest('POST_ASSIGN', postAssign);
}

export {
	watchGetBooks,
	watchDeleteBook,
	watchPostBook,
	watchPostAssign
};