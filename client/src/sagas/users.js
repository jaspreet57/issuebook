import {takeLatest} from 'redux-saga';

// below are some saga effects

import {put, select, call} from 'redux-saga/effects';

import {getUsers as getUsersAction, getUsersSuccess, getUsersFailure, deleteUserSuccess, deleteUserFailure, postUserSuccess, postUserFailure} from '../actions/users';


const fetchUsers = () => {
	return fetch('http://localhost:8080/users', {
			headers: new Headers({
				'Content-Type': 'application/json'
			})
		})
		.then(response => response.json())
};


const selectedUsers = (state) => {
	return state.getIn(['users', 'list']).toJS();
};


const deleteServerUser = (id) => {
	return fetch(`http://localhost:8080/users/${id}`, {
			headers: new Headers({
				'Content-Type': 'application/json'
			}),
			method: 'DELETE',
		})
		.then(response => response.json());
};

const postServerUser = (user) => {
	return  fetch('http://localhost:8080/users', {
				headers: new Headers({
					'Content-Type': 'application/json'
				}),
				method: 'POST',
				body: JSON.stringify(user)
			})
			.then(response => response.json());
};


const getUserForm = (state) => {
	return state.getIn(['form', 'user']).toJS();
}


function* getUsers(){
	try {
		const users = yield call(fetchUsers);
		yield put(getUsersSuccess(users));
	}catch (err){
		yield put(getUsersFailure());
	}
}


function* deleteUser (action){
	const {id} = action;
	const users = yield select(selectedUsers);
	try {
		yield call(deleteServerUser, id);

		yield put(deleteUserSuccess(users.filter(user => user._id !== id)));
	} catch (e) {
		yield put(deleteUserFailure());
	}
}


function* postUser(){
	const user = yield select(getUserForm);

	const newUser = Object.assign({}, user.values);

	try {
		yield call(postServerUser, newUser);
		yield put(getUsersAction());
	}catch (e){
		yield put(postUserFailure());
	}
}



function* watchGetUsers(){
	yield takeLatest('GET_USERS', getUsers);
}


function* watchDeleteUser() {
	yield takeLatest('DELETE_USER', deleteUser);
}


function* watchPostUser(){
	yield takeLatest('POST_USER', postUser);
}

export {
	watchGetUsers,
	watchDeleteUser,
	watchPostUser
};