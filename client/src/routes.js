import React from 'react';

import {Provider} from 'react-redux';
import configureStore from './store';

import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import Wrapper from './components/Wrapper';
import BookContainer from './containers/BookContainer';
import AddBook from './containers/AddBook';
import UserContainer from './containers/UserContainer';
import AddUser from './containers/AddUser';
import AssignBook from './containers/AssignBook';
import UserProfile from './containers/UserProfile';

const store = configureStore();

const routes = (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={Wrapper}>
        <IndexRoute component={BookContainer} />
        <Route path="/addbook" component={AddBook} />
        <Route path="/users" component={UserContainer} />
        <Route path="/adduser" component={AddUser} />
        <Route path="/assign/:id" component={AssignBook} />
        <Route path="/user/:id" component={UserProfile} />
      </Route>
    </Router>
  </Provider>
);

export default routes;
