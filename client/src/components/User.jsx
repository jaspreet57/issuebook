import React, { PureComponent } from 'react';
import { Link } from 'react-router';

export default class User extends PureComponent {
  render () {
    const { _id, i, name, username, deleteUser } = this.props;
    return (
      <div className="col-md-4">
        <div className="thumbnail">
          <div className="thumbnail-frame">
          </div>
          <div className="caption">
            <h6>{username}</h6>
            <h5>{name}</h5>

            <div className="btn-group" role="group" aria-label="...">
              <Link to={'/user/'+_id} className="btn btn-success">View Profile</Link>
              <button className="btn btn-danger" role="button" onClick={() => deleteUser(_id)}>Delete</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}