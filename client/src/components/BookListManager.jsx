import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import Book from './Book';

export default class BookListManager extends PureComponent {
	render(){
		const {books, searchBar, setSearchBar, deleteBook} = this.props;
		return (
			<div className="container">
		        <div className="row text-left">
		          <Link to="/addbook" className="btn btn-danger">Add a new Book!</Link>
		        </div>
		        <br />
		        <div className="row">
		          <input
		            type="search" placeholder="Search Books" className="form-control search-bar" onKeyUp={setSearchBar} />
		        </div>
		        <br /><br />
		        <div className="row">
		        {
		          books
		            .filter(book => book.name.toLowerCase().includes(searchBar))
		            .map((book, i) => {
		              return (
		                <Book  {...book}
		                  key={book._id}
		                  i={i}
		                  deleteBook={deleteBook}
		                />
		              );
		            })
		        }
		        </div>
		    </div>
		);
	}
}