import React, {PureComponent} from 'react';
import {Link} from 'react-router';

export default class Wrapper extends PureComponent {
	active(path){
		if(this.props.location.pathname === path){
			return 'active';
		}
	}

	render(){
		return (
			<div>
				<br />
				<div className="container">
					<div className="header clearfix">
				        <nav>
				          <ul className="nav nav-pills pull-right">
				           <li className={this.active('/')}><Link to="/">Home</Link></li>
				           <li className={this.active('/users')}><Link to="/users">User Management</Link></li>
				          </ul>
				        </nav>
				        <h3 className="text-muted">Issue Book</h3>
				    </div>
				</div>
				{this.props.children}
			</div>
		);
	}
}