import React, { PureComponent } from 'react';
import { Link } from 'react-router';

export default class Book extends PureComponent {
  render () {
    const { _id, i, name, isAssigned, deleteBook } = this.props;
    return (
      <div className="col-md-4">
        <div className="thumbnail">
          <div className="thumbnail-frame">
          </div>
          <div className="caption">
            <h5>{name}</h5>
            <div className="btn-group" role="group" aria-label="...">
              <Link to={'/assign/'+_id} disabled={isAssigned} className="btn btn-success">Assign</Link>
              <button className="btn btn-danger" role="button" disabled={isAssigned} onClick={() => deleteBook(_id)}>Delete</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}