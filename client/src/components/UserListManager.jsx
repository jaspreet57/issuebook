import React, { PureComponent } from 'react';
import { Link } from 'react-router';
import User from './User';

export default class UserListManager extends PureComponent {
	render(){
		const {users, searchBar, setSearchBar, deleteUser} = this.props;
		return (
			<div className="container">
		        <div className="row text-left">
		          <Link to="/adduser" className="btn btn-danger">Add a new User!</Link>
		        </div>
		        <br />
		        <div className="row">
		          <input
		            type="search" placeholder="Search Users" className="form-control search-bar" onKeyUp={setSearchBar} />
		        </div>
		        <br /><br />
		        <div className="row">
		        {
		          users
		            .filter(user => user.username.toLowerCase().includes(searchBar))
		            .map((user, i) => {
		              return (
		                <User  {...user}
		                  key={user._id}
		                  i={i}
		                  deleteUser={deleteUser}
		                />
		              );
		            })
		        }
		        </div>
		    </div>
		);
	}
}
