import React, {PureComponent} from 'react';
import { Link } from 'react-router';
import {Field, reduxForm} from 'redux-form/immutable';


class BookForm extends PureComponent{
  render(){
    return (
      <div className="container">
         <Link to="/" className="btn btn-info">Back</Link>
         <form name="product-form" action="" onSubmit={this.props.handleSubmit} noValidate>
                  <div className="form-group text-left">
                        <label htmlFor="caption">Name</label>
                        <Field
                          name="name"
                          type="text"
                          className="form-control"
                          component="input"
                          placeholder="Enter the title"
                          />
                  </div>
                  <button type="submit" className="btn btn-submit btn-block">Add Book</button>
          </form>
      </div>
    );
  }
}

export default reduxForm({form : 'book'})(BookForm);
