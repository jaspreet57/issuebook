import React, { Component } from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Immutable from 'immutable';

import { Link } from 'react-router';
import { hashHistory } from 'react-router';
import * as usersActionCreators from '../actions/users';
import * as assignActionCreators from '../actions/assign';

class AssignBook extends Component {
  constructor (props) {
    super(props);
    // Initial state
    this.state = {searchBar : ''};
    // Bind this (context) to the functions to be passed down to the children components
    this.submit = this.submit.bind(this);
    this.setSearchBar = this.setSearchBar.bind(this);
  }
  
  componentDidMount(){
		this.getUser();
	}
  
  getUser(){
		this.props.usersActions.getUsers();
	}
  
  setSearchBar (event){
		this.setState({ searchBar: event.target.value.toLowerCase() });
	}

  
  submit (userid) {
    this.props.assignActions.postAssign(userid, this.props.params.id);
    hashHistory.push('/'); 
  }


  render () {
    const {searchBar} = this.state;
    const {users} = this.props;
    
    return (
      <div className="container">
         <Link to="/" className="btn btn-info">Back</Link>
         <h1>Select User to assign book:</h1>
          <div className="row">
		          <input
		            type="search" placeholder="Search Users" className="form-control search-bar" onKeyUp={this.setSearchBar} />
		      </div>
          <div className="row">
            <br/>
		        {
		          users
		            .filter(user => user.username.toLowerCase().includes(searchBar))
		            .map((user, i) => {
		              return (
                    <div key={user._id}><span>{user.name} &nbsp; ({user.username})</span> &nbsp; &nbsp; &nbsp;<button className="btn btn-success" onClick={() => this.submit(user._id)}>Assign</button><br /><hr /></div>
		              );
		            })
		        }
		        </div>
      </div>
    );
  }
}


function mapStateToProps (state){
  return {
    users: state.getIn(['users', 'list'], Immutable.List()).toJS()
  };
}


function mapDispatchToProps (dispatch){
  return {
    usersActions: bindActionCreators(usersActionCreators, dispatch),
    assignActions: bindActionCreators(assignActionCreators, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignBook);
