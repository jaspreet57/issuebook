import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Immutable from 'immutable';
import * as usersActionCreators from '../actions/users';

import UserListManager from '../components/UserListManager';


class UserContainer extends Component {
	constructor (props){
		super(props);


		this.deleteUser = this.deleteUser.bind(this);
		this.setSearchBar = this.setSearchBar.bind(this);
	}

	componentDidMount(){
		this.getUsers();
	}

	getUsers(){
		this.props.usersActions.getUsers();
	}

	deleteUser(id){
		this.props.usersActions.deleteUser(id);
	}

	setSearchBar (event){
		this.props.usersActions.setUserSearchBar(event.target.value.toLowerCase());
	}

	render(){
		const {users, searchBar} = this.props;

		return (
		        <UserListManager 
		        	users={users} 
		        	searchBar={searchBar} 
		        	setSearchBar={this.setSearchBar} 
		        	deleteUser={this.deleteUser} 
		        	/>
		);
	}

}


function mapStateToProps (state){
	return {
		users: state.getIn(['users', 'list'], Immutable.List()).toJS(),
		searchBar: state.getIn(['users', 'searchBar'], '')
	};
}


function mapDispatchToProps (dispatch){
	return {
		usersActions: bindActionCreators(usersActionCreators, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);
