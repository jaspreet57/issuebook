import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Immutable from 'immutable';
import * as profileActionCreators from '../actions/userprofile';


class UserProfile extends Component {
	constructor (props){
		super(props);

		this.deleteBook = this.deleteBook.bind(this);
	}

	componentDidMount(){
		this.getUser();
		this.getAssignments();
	}

	getUser(){
		this.props.profileActions.getUserProfile(this.props.params.id);
	}

	getAssignments(){
		this.props.profileActions.getAssignments(this.props.params.id);
	}


	deleteBook(id){
		this.props.profileActions.deleteAssignment(id);
	}


	render(){
		const {fullname, username, assignments} = this.props;

		return (
			<div className="container">
				<div className="row text-center">
					<h1>{fullname}</h1>
					<h4>{username}</h4>
				</div>
				<h2>Books Assigned</h2>
				<ul>
				{
					assignments
		            .map((assignment, i) => {
		              return (
		                <li key={i}>bookname: <strong> {assignment.bookid.name} </strong> &nbsp;&nbsp;&nbsp; <button className="btn btn-danger" onClick={() => this.deleteBook(assignment._id)}>Unassign</button></li>
		              );
		            })

		        }
				</ul>
			</div>
		);
	}

}


function mapStateToProps (state){
	return {
		fullname: state.getIn(['userprofile', 'user', 'name'], ''),
		username: state.getIn(['userprofile', 'user', 'username'], ''),
		assignments: state.getIn(['userprofile', 'assignments'], Immutable.List()).toJS()
	};
}


function mapDispatchToProps (dispatch){
	return {
		profileActions: bindActionCreators(profileActionCreators, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
