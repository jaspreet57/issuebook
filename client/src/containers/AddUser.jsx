import React, { Component } from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as usersActionCreators from '../actions/users';

import { hashHistory } from 'react-router';

import UserForm from '../components/UserForm';

class AddUser extends Component {
  constructor (props) {
    super(props);
    
    this.submit = this.submit.bind(this);
  }

  submit (event) {
    event.preventDefault();
    this.props.usersActions.postUser();
    hashHistory.push('/users');
  }


  render () {
    return (
      <UserForm
        handleSubmit={this.submit}
        />
    );
  }
}


function mapStateToProps(state){
  return {

  };
}

function mapDispatchToProps(dispatch){
  return {
    usersActions: bindActionCreators(usersActionCreators, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
