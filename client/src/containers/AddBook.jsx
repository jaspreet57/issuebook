import React, { Component } from 'react';

//redux things
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as booksActionCreators from '../actions/books';

// for integrating with redux-form


import { hashHistory } from 'react-router';

import BookForm from '../components/BookForm';

class AddBook extends Component {
  constructor (props) {
    super(props);
    // Bind this (context) to the functions to be passed down to the children components
    this.submit = this.submit.bind(this);
  }

  submit (event) {
      event.preventDefault();
      this.props.booksActions.postBook();
      hashHistory.push('/');
  }


  render () {
    return (
      <BookForm
        handleSubmit={this.submit}
        />
    );
  }
}


function mapStateToProps(state){
  return {

  };
}

function mapDispatchToProps(dispatch){
  return {
    booksActions: bindActionCreators(booksActionCreators, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(AddBook);