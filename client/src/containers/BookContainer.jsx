import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Immutable from 'immutable';
import * as booksActionCreators from '../actions/books';

import BookListManager from '../components/BookListManager';


class BookContainer extends Component {
	constructor (props){
		super(props);

		this.deleteBook = this.deleteBook.bind(this);
		this.setSearchBar = this.setSearchBar.bind(this);
	}

	componentDidMount(){
		this.getBooks();
	}

	getBooks(){
		this.props.booksActions.getBooks();
	}

	deleteBook(id){
		this.props.booksActions.deleteBook(id);
	}

	setSearchBar (event){
		this.props.booksActions.setBookSearchBar(event.target.value.toLowerCase());
	}

	render(){
		const { books, searchBar } = this.props;

		return (
		        <BookListManager 
		            books={books} 
		            searchBar={searchBar} 
		            setSearchBar={this.setSearchBar} 
		            deleteBook={this.deleteBook}
		        	/>
		);
	}

}


function mapStateToProps (state){
	return {
		books: state.getIn(['books', 'list'], Immutable.List()).toJS(),
		searchBar: state.getIn(['books', 'searchBar'], '')
	};
}


function mapDispatchToProps (dispatch){
	return {
		booksActions: bindActionCreators(booksActionCreators, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(BookContainer);
