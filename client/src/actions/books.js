/* Actions for books */

// GET_BOOKS action creator
function getBooks(){
	return {
		type: 'GET_BOOKS'
	};
}


// On successfully getting books
function getBooksSuccess(books){
	return {
		type: 'GET_BOOKS_SUCCESS',
		books
	};
}


// On failure while getting books
function getBooksFailure() {
	return {
		type: 'GET_BOOKS_FAILURE'
	};
}


// set search bar
function setBookSearchBar(keyword){
	return {
		type: 'SET_BOOK_SEARCH_BAR',
		keyword
	}
}


function deleteBook(id){
	return {
		type: 'DELETE_BOOK',
		id
	};
}


function deleteBookSuccess(books) {
	return {
		type: 'DELETE_BOOK_SUCCESS',
		books
	};
}


function deleteBookFailure() {
	return {
		type: 'DELETE_BOOK_FAILURE'
	};
}


function postBook(){
	return{
		type: 'POST_BOOK'
	};
}

function postBookSuccess(){
	return{
		type: 'POST_BOOK_SUCCESS'
	};
}

function postBookFailure(){
	return{
		type: 'POST_BOOK_FAILURE'
	};
}


export {
	getBooks,
	getBooksSuccess,
	getBooksFailure,
	deleteBook,
	deleteBookSuccess,
	deleteBookFailure,
	postBook,
	postBookSuccess,
	postBookFailure,
	setBookSearchBar
};
