/* Actions for userprofile */

// GET_USER_PROFILE action creator
function getUserProfile(id){
	return {
		type: 'GET_USER_PROFILE',
    	id
	};
}


// On successfully getting user
function getUserProfileSuccess(user){
	return {
		type: 'GET_USER_PROFILE_SUCCESS',
		user
	};
}


// On failure while getting user
function getUserProfileFailure() {
	return {
		type: 'GET_USER_PROFILE_FAILURE'
	};
}


function getAssignments(id){
	return {
		type: 'GET_ASSIGNMENTS',
    	id
	};
}



function getAssignmentsSuccess(assignments){
	return {
		type: 'GET_ASSIGNMENTS_SUCCESS',
		assignments
	};
}



function getAssignmentsFailure() {
	return {
		type: 'GET_ASSIGNMENTS_FAILURE'
	};
}


function deleteAssignment(id){
	return {
		type: 'DELETE_ASSIGNMENT',
		id
	};
}


function deleteAssignmentSuccess(assignments) {
	return {
		type: 'DELETE_ASSIGNMENT_SUCCESS',
		assignments
	};
}


function deleteAssignmentFailure() {
	return {
		type: 'DELETE_ASSIGNMENT_FAILURE'
	};
}



export {
	getUserProfile,
  getUserProfileSuccess,
  getUserProfileFailure,
  getAssignments,
  getAssignmentsSuccess,
  getAssignmentsFailure,
  deleteAssignment,
  deleteAssignmentSuccess,
  deleteAssignmentFailure
};
