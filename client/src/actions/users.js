/* Actions for users */

// GET_USERS action creator
function getUsers(){
	return {
		type: 'GET_USERS'
	};
}


// On successfully getting users
function getUsersSuccess(users){
	return {
		type: 'GET_USERS_SUCCESS',
		users
	};
}


// On failure while getting users
function getUsersFailure() {
	return {
		type: 'GET_USERS_FAILURE'
	};
}


// set search bar //this function is going to be duplicate one....do something about it..
function setUserSearchBar(keyword){
	return {
		type: 'SET_USER_SEARCH_BAR',
		keyword
	}
}


function deleteUser(id){
	return {
		type: 'DELETE_USER',
		id
	};
}


function deleteUserSuccess(users) {
	return {
		type: 'DELETE_USER_SUCCESS',
		users
	};
}


function deleteUserFailure() {
	return {
		type: 'DELETE_USER_FAILURE'
	};
}


function postUser(){
	return{
		type: 'POST_USER'
	};
}

function postUserSuccess(){
	return{
		type: 'POST_USER_SUCCESS'
	};
}

function postUserFailure(){
	return{
		type: 'POST_USER_FAILURE'
	};
}


export {
	getUsers,
	getUsersSuccess,
	getUsersFailure,
	deleteUser,
	deleteUserSuccess,
	deleteUserFailure,
	postUser,
	postUserSuccess,
	postUserFailure,
	setUserSearchBar
};
