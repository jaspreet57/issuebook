function postAssign(userid, bookid){
	return{
		type: 'POST_ASSIGN',
		userid,
		bookid
	};
}

function postAssignFailure(){
	return{
		type: 'POST_ASSIGN_FAILURE'
	};
}

export{
	postAssign,
	postAssignFailure
};