import mongoose from 'mongoose';
import User from './users';
import Book from './books';
const Schema = mongoose.Schema;


const assignRecord = new Schema(
	{
		userid: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
		bookid: {type: mongoose.Schema.Types.ObjectId, ref: 'Book'}
	}
);

export default mongoose.model('Assign', assignRecord);
