import mongoose from 'mongoose';
const Schema = mongoose.Schema;


const ibBook = new Schema(
	{
		name: String,
		isAssigned: Boolean
	}
);

export default mongoose.model('Book', ibBook);
