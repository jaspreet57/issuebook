import mongoose from 'mongoose';
const Schema = mongoose.Schema;


const ibUser = new Schema(
	{
		name: String,
		username: String
	}
);

export default mongoose.model('User', ibUser);
