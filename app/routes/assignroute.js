import User from '../models/users';
import Book from '../models/books';
import Assign from '../models/assignrecord';


const assignBook = (req, res) => {
  let assignment = Object.assign(new Assign(), req.body);

  Assign.find({'userid': assignment.userid}, null, {}, (err, assignments) => {
		if (err) {
			res.send(err);
		}
		if(assignments.length <3){
			Book.findOneAndUpdate({'_id':assignment.bookid}, {'isAssigned': true}, {upsert:true}, function(err, doc){
			    if (err) return res.send(500, { error: err });
			    assignment.save(err => {
				    if (err) {
				      res.send(err);
				    }
				    res.json({ message: 'Book is assigned to User' });
				});
			});
			
		}else{
			res.json({message : 'More than 3 books cannot be assigned to user'});
		}
	});
  
};


const getAssignedBooks = (req, res)=> {
	Assign.find({'userid': req.params.id}).populate("bookid").exec(function(err, assignments){
		if (err) {
			res.send(err);
		}
		console.log(assignments);
		res.json(assignments);
	});
};




const deleteAssign = (req, res) => {
	Assign.findOne({_id: req.params.id}, (err, assignment)=>{
		if(err) {
			res.send(err);
		}
		Book.findOneAndUpdate({'_id':assignment.bookid}, {'isAssigned': false}, {upsert:true}, function(err, doc){
			    if (err) return res.send(500, { error: err });
			    Assign.remove(
				    { _id: req.params.id },
				    err => {
				      if (err) {
				        res.send(err);
				      }
				      res.json({ message: 'successfully removed' });
				    }
				);
		});
	});


};

export {
	assignBook,
	getAssignedBooks,
	deleteAssign
};
