import Book from '../models/books';

const getBooks = (req, res) => {
	Book.find(null, null, { sort: { name : 1 } }, (err, books) => {
		if (err) {
			res.send(err);
		}
		res.json(books);
	});
}

const getBook = (req, res) => {
	const { id } = req.params;
	Book.findById(id, (err, book) => {
		if (err) {
			res.send(err);
		}
		res.json(book);
	});
}

const postBook = (req, res) => {
  	let book = Object.assign(new Book(), req.body);

  	book.save(err => {
	    if (err) {
	      res.send(err);
	    }
	    res.json({ message: 'new book created' });
	});

  
};

const deleteBook = (req, res) => {
  Book.remove(
    { _id: req.params.id },
    err => {
      if (err) {
        res.send(err);
      }else{
      		res.json({ message: 'successfully deleted' });
  	  }
    }
  );
};

export {
	getBooks,
	getBook,
	postBook,
	deleteBook
};
