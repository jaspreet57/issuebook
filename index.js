import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import morgan from 'morgan';

import User from './app/models/users';
import Book from './app/models/books';
import { getUsers, getUser, postUser, deleteUser } from './app/routes/users';
import { getBooks, getBook, postBook, deleteBook } from './app/routes/books';
import { assignBook, getAssignedBooks, deleteAssign } from './app/routes/assignroute.js';



const app = express();
const port = process.env.PORT || 8080;

// DB connection through Mongoose
const options = {
  server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
  replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } }
};
mongoose.Promise = global.Promise;
mongoose.connect('localhost', options);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// Body parser and Morgan middleware
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));

// directory of assets
app.use(express.static(__dirname + '/client/dist'));

// Enable CORS to enable make HTTP request from webpack-dev-server
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



// API routes
app.route('/books')
	.post(postBook)
	.get(getBooks);

app.route('/books/:id')
	.get(getBook)
	.delete(deleteBook);

app.route('/users')
	.post(postUser)
	.get(getUsers);
	
app.route('/users/:id')
	.get(getUser)
	.delete(deleteUser);

app.route('/assign/')
	.post(assignBook);

app.route('/assign/user/:id')
	.get(getAssignedBooks);

app.route('/assign/:id')
	.delete(deleteAssign);


// ...For all the other requests just sends back the Homepage
app.route("*").get((req, res) => {
	res.sendFile('client/dist/index.html', { root: __dirname });
});

app.listen(port);

console.log(`listening on port ${port}`);
