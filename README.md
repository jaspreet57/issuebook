Instructions:

Make Sure you have following tools:
1. mongodb
2. node, npm
3. yarn   (install by "npm install -g yarn")


Launch in Production mode:

1. after cloning first install dependencies:
	```bash
    yarn install
    ```

2. Build React Client App
	```bash
    yarn build-client
    ```

3. Run Server
	```bash
    yarn server
    ```

4. Now Node Server will be running at port 8080:<br>
	visit : http://localhost:8080






Launch in Development Mode:

1. first 
```bash
 yarn install
```

2. now run both client 'webpack' and node server in seperate terminal
```bash
yarn start-client
yarn server
```
3. Now launch http://localhost:3000 in browser
4. In Development Mode, web page will auto refresh in case you changed any react or redux code.




Note: 
Both above methods will work fine for localhost testing only. You can access app over network. To deploy app to any domain, url have to be changed in redux-sagas files.
Later I will make url configurable.